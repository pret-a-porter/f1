import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import Typography from "@material-ui/core/Typography";
import Skeleton from "@material-ui/lab/Skeleton";
import React, { useEffect, useState } from "react";

import { getSeason, StandingsList } from "../api/season";
import { Driver } from "../models/driver";

interface Props {
  year: number;

  onSelect(yar: number, winner: Driver, races: number): void;
}

export const Season: React.FC<Props> = ({ year, onSelect }) => {
  const [data, setData] = useState<StandingsList | undefined>();

  useEffect(() => {
    getSeason(year).then(setData);
  }, [year]);

  const winner = data?.DriverStandings[0].Driver;

  return (
    <Card style={{ marginRight: 10, marginBottom: 10, width: 200 }}>
      <CardHeader
        avatar={
          winner ? (
            <Avatar>
              {winner.givenName.charAt(0)}
              {winner.familyName.charAt(0)}
            </Avatar>
          ) : (
            <Skeleton animation="wave" width={150} />
          )
        }
        title={winner ? `${winner.givenName} ${winner.familyName}` : ""}
      />
      <CardContent>
        <Typography color="textSecondary">
          Season
        </Typography>
        <Typography variant="h5" component="h2">
          {year}
        </Typography>
      </CardContent>
      {!!winner && (
        <CardActions>
          <Button
            onClick={() => onSelect(year, winner, Number(data?.round))}
            size="small"
          >
            Show races
          </Button>
        </CardActions>
      )}
    </Card>
  );
};
