import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import React, { useEffect, useState } from "react";

import { Driver } from "../models/driver";
import { PageLayout } from "./PageLayout";
import { Race } from "./Race";
import { Season } from "./Season";

export const WorldChampionsList: React.FC = () => {
  const [isSeasonListVisible, setIsSeasonListVisible] = useState(true);
  const [year, setYear] = useState(SEASONS[0]);
  const [races, setRaces] = useState<number[]>([]);
  const [winner, setWinner] = useState<Driver | undefined>();

  const handleSelectSeason = (year: number, winner: Driver, count: number) => {
    setYear(year);
    setRaces(Array.from(Array(count).keys()));
    setWinner(winner);
    setIsSeasonListVisible(false);
  };

  return (
    <PageLayout>
      <div
        style={{
          display: isSeasonListVisible ? "flex" : "none",
          flexWrap: "wrap",
        }}
      >
        {SEASONS.map((item) => (
          <Season onSelect={handleSelectSeason} key={item} year={item} />
        ))}
      </div>
      {!isSeasonListVisible && (
        <React.Fragment>
          <Button
            onClick={() => {
              setIsSeasonListVisible(true);
            }}
          >
            Back
          </Button>
          <List>
            {races.map((item) => (
              <Race
                seasonWinner={winner}
                key={`${year}-${item}`}
                year={year}
                round={item + 1}
              />
            ))}
          </List>
        </React.Fragment>
      )}
    </PageLayout>
  );
};

const SEASONS: ReadonlyArray<number> = [
  2005,
  2006,
  2007,
  2008,
  2009,
  2010,
  2011,
  2012,
  2013,
  2014,
  2015,
];
