import Avatar from "@material-ui/core/Avatar";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import EmojiEventsIcon from "@material-ui/icons/EmojiEvents";
import Skeleton from "@material-ui/lab/Skeleton";
import React, { useEffect, useState } from "react";

import { getResults, RaceResults } from "../api/results";
import { Driver } from "../models/driver";

interface Props {
  year: number;
  round: number;
  seasonWinner?: Driver;
}

export const Race: React.FC<Props> = ({ year, round, seasonWinner }) => {
  const [data, setData] = useState<RaceResults | undefined>();

  useEffect(() => {
    getResults(year, round).then(setData);
  }, [year, round]);

  return (
    <ListItem>
      <ListItemAvatar>
        <Avatar
          style={{
            backgroundColor:
              seasonWinner &&
              data &&
              seasonWinner.code === data.Results[0].Driver.code
                ? "gold"
                : "gray",
          }}
        >
          <EmojiEventsIcon />
        </Avatar>
      </ListItemAvatar>
      {data ? (
        <ListItemText
          primary={
            <React.Fragment>
              <i>{data.raceName}</i>
              &nbsp;|&nbsp;
              <span>
                {data.Results[0].Driver.givenName}&nbsp;
                {data.Results[0].Driver.familyName}
              </span>
            </React.Fragment>
          }
          secondary={new Intl.DateTimeFormat("en", {
            day: "numeric",
            month: "long",
            year: "numeric",
          }).format(new Date(data.date))}
        />
      ) : (
        <Skeleton animation="wave" width={200} />
      )}
    </ListItem>
  );
};
