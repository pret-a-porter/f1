import AppBar from "@material-ui/core/AppBar";
import Container from "@material-ui/core/Container";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";

import React from "react";

export const PageLayout: React.FC = ({ children }) => {
  return (
    <div>
      <CssBaseline />
      <AppBar position="static">
        <Toolbar variant="dense">
          <Container maxWidth="lg">
            <Typography variant="h6" color="inherit">
              Formula 1
            </Typography>
          </Container>
        </Toolbar>
      </AppBar>
      <Container maxWidth="lg">
        <Box mt={2}>
          {children}
        </Box>
      </Container>
    </div>
  );
};
