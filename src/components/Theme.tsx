import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import React from "react";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#ff1801",
    },
  },
  typography: {
    fontFamily: "'Titillium Web', sans-serif",
  },
});

export const Theme: React.FC = ({ children }) => {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};
