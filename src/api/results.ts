import { Constructor } from '../models/constructor';
import { Driver } from '../models/driver';

export interface RaceResults {
  date: string;
  raceName: string;
  Results: ReadonlyArray<Result>;
  time: string;
  url: string;
}

interface Result {
  Constructor: Constructor;
  Driver: Driver;
  number: string;
  points: string;
  position: string;
  positionText: string;
}

export async function getResults(year: number, round: number) {
  try {
    const response = await fetch(
      `http://ergast.com/api/f1/${year}/${round}/results.json`
    );
    const { MRData } = await response.json();
    return MRData.RaceTable.Races[0] as RaceResults;
  } catch (error) {
    console.error(error);
  }
}
