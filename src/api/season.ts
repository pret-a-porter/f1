import { Driver } from '../models/driver';

export interface StandingsList {
  DriverStandings: ReadonlyArray<DriverStanding>;
  round: string;
  season: string;
}

export interface DriverStanding {
  Driver: Driver;
  points: string;
  position: string;
  positionText: string;
  wins: string;
}

export async function getSeason(year: number) {
  try {
    const response = await fetch(
      `http://ergast.com/api/f1/${year}/driverStandings.json`
    );
    const { MRData } = await response.json();
    return MRData.StandingsTable.StandingsLists[0] as StandingsList;
  } catch (error) {
    console.error(error);
  }
}
