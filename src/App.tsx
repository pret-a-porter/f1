import React from "react";

import { Theme } from "./components/Theme";
import { WorldChampionsList } from "./components/WorldChampionsList";

export const App: React.FC = () => {
  return (
    <Theme>
      <WorldChampionsList />
    </Theme>
  );
};
